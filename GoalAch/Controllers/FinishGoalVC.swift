//
//  FinishGoalVC.swift
//  GoalAch
//
//  Created by phuongzzz on 10/20/18.
//  Copyright © 2018 phuongzzz. All rights reserved.
//

import UIKit
import CoreData

let appDelegate = UIApplication.shared.delegate as? AppDelegate

class FinishGoalVC: UIViewController {

    @IBOutlet weak var createGoalBtn: UIButton!
    @IBOutlet weak var goalCompletionValueLabel: UILabel!
    @IBOutlet weak var stepper: UIStepper!

    var goalDescription: String!
    var goalType: GoalType!
    
    func initData(description: String, type: GoalType) {
        self.goalDescription = description
        self.goalType = type
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        createGoalBtn.bindToKeyboard()
    }
    
    @IBAction func createGoalBtnPressed() {
        let goalCompletionValue = Int32(goalCompletionValueLabel.text!)
        
        if goalCompletionValue != 0 {
            self.save { (complete) in
                if complete {
                    dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    @IBAction func steppterValueChanged() {
        self.goalCompletionValueLabel.text = String(describing: Int32(stepper.value))
    }
    
    @IBAction func backBtnPressed() {
        dismissDetail()
    }
    
    func save(completion: (_ finished: Bool) -> ()) {
        guard let managedContext = appDelegate?.persistentContainer.viewContext else { return }
        let goal = Goal(context: managedContext)
        
        goal.goalDescription = self.goalDescription
        goal.goalType = self.goalType.rawValue
        goal.goalCompletionValue = Int32(goalCompletionValueLabel.text!)!
        goal.goalProgress = Int32(0)
        
        do {
            try managedContext.save()
            print("successfully")
            completion(true)
        } catch {
            debugPrint("could not save \(error.localizedDescription)")
            completion(false)
        }
    }
}
