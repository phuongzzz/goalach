//
//  ViewController.swift
//  GoalAch
//
//  Created by phuongzzz on 10/19/18.
//  Copyright © 2018 phuongzzz. All rights reserved.
//

import UIKit
import CoreData

class GoalsVC: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var goals: [Goal] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.fetchCoreDataObjects()
        tableView.reloadData()
    }
    
    func fetchCoreDataObjects() {
        self.fetch() {
            (complete) in
            if complete {
                if goals.count > 0 {
                    tableView.isHidden = false
                } else {
                    tableView.isHidden = true
                }
            }
        }
    }
    
    @IBAction func addGoalBtnPressed() {
        guard let createGoalVC = storyboard?.instantiateViewController(withIdentifier: "CreateGoalVC") else {
            return
        }
        presentDetail(createGoalVC)
    }
}

extension GoalsVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return goals.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "goalCell") as? GoalCell else {
            return UITableViewCell()
        }
        
        let goal = goals[indexPath.row]
        
        cell.configureCell(goal: goal)
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
//        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete") { (rowAction, indexPath) in
//            self.removeGoal(atIndexPath: indexPath) { (done) in
//                if done {
//                    self.fetchCoreDataObjects()
//                    tableView.deleteRows(at: [indexPath], with: .automatic)
//                }
//            }
//        }
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete") { (rowAction, indexPath) in
            let alertController = UIAlertController(title: nil, message: "Are you sure to delete?", preferredStyle: .actionSheet)
            let confirmDeleteAction = UIAlertAction(title: "Yes, I'm sure", style: .destructive, handler: { (UIAlertAction) in
                self.removeGoal(atIndexPath: indexPath, completion: { (done) in
                    if done {
                        self.fetchCoreDataObjects()
                        tableView.deleteRows(at: [indexPath], with: .automatic)
                    }
                })
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alertController.addAction(confirmDeleteAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true)
        }
        deleteAction.backgroundColor = #colorLiteral(red: 0.9058823529, green: 0.2980392157, blue: 0.2352941176, alpha: 1)
        
        let updateAction = UITableViewRowAction(style: .normal, title: "Add") { (rowAction, indexPath) in
            self.setProgress(at: indexPath, completion: { (done) in
                if done {
                    tableView.reloadRows(at: [indexPath], with: .automatic)
                }
            })
        }
        updateAction.backgroundColor = #colorLiteral(red: 0.1803921569, green: 0.8, blue: 0.4431372549, alpha: 1)
        
        return [deleteAction, updateAction]
    }
}

extension GoalsVC {
    func fetch(completion: (Bool) -> ()) {
        guard let managedContext = appDelegate?.persistentContainer.viewContext else { return }
        let fetchRequest = NSFetchRequest<Goal>(entityName: "Goal")
        
        do {
            goals = try managedContext.fetch(fetchRequest)
            debugPrint("successfully fetched data")
            completion(true)
        } catch {
            debugPrint("Could not fetch \(error.localizedDescription)")
            completion(false)
        }
    }
    
    func removeGoal(atIndexPath indexPath: IndexPath, completion: (Bool) -> ()) {
        guard let managedContext = appDelegate?.persistentContainer.viewContext else { return }
        managedContext.delete(goals[indexPath.row])
        do {
            try managedContext.save()
            debugPrint("Deleted successfully")
            completion(true)
        } catch {
            debugPrint("Could not delete data \(error.localizedDescription)")
            completion(false)
        }
    }
    
    func setProgress(at indexPath: IndexPath, completion: (Bool) -> ()) {
        guard let managedContext = appDelegate?.persistentContainer.viewContext else { return }
        let choosenGoal = goals[indexPath.row]
        
        if choosenGoal.goalProgress < choosenGoal.goalCompletionValue {
            choosenGoal.goalProgress += 1
        } else {
            return
        }
        
        do {
            try managedContext.save()
            debugPrint("update progress successfully")
            completion(true)
        } catch {
            debugPrint("can not update progress \(error.localizedDescription)")
            completion(false)
        }
    }
}
