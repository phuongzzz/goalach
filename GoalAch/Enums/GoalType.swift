//
//  GoalType.swift
//  GoalAch
//
//  Created by phuongzzz on 10/20/18.
//  Copyright © 2018 phuongzzz. All rights reserved.
//

import Foundation

enum GoalType: String {
    case longTerm = "Long Term"
    case shortTerm = "Short Term"
}
