//
//  UIButtonForExt.swift
//  GoalAch
//
//  Created by phuongzzz on 10/20/18.
//  Copyright © 2018 phuongzzz. All rights reserved.
//

import UIKit

extension UIButton {
    func setSelectedColor()  {
        self.backgroundColor = #colorLiteral(red: 0.5725490196, green: 0.6588235294, blue: 0.8196078431, alpha: 1)
    }
    func setDeselectedColor()  {
        self.backgroundColor = #colorLiteral(red: 0.6375300288, green: 0.7201424241, blue: 0.8542764783, alpha: 0.7028039384)
    }
}

